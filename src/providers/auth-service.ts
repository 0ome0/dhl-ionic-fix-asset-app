import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

//let apiUrl = "http://localhost/PHP-Slim-Restful/api/";
// let apiUrl = 'https://api.thewallscript.com/restful/';
// let apiUrl =    'http://localhost/dhl-fa-v3/api/mobile/';
// let apiUrl = 'https://thlogis-e.com/inventory/api/mobile/';

// let apiUrl = 'https://service-imsp.com/dhl-fa-v3/api/mobile/';
// let apiUrl =    'http://localhost/dhl-fa-v3/api/mobile/'; 

// let apiUrl =    'http://localhost/dhlecom-fixasset/api/mobile/'; 
// let apiUrl =    'https://d7a5cb49.ngrok.io/dhlecom-fixasset/api/mobile/'
let apiUrl =    'https://www.ecom-logistic.com/fa/api/mobile/'

// function name

let login = 'login';
let create_new_count = 'create_new_count_no';
let get_count_list_by_user = 'get_count_list_by_user';
let get_asset_list_by_count_no = 'get_asset_list_by_count_no';
let get_asset_detail = 'get_asset_detail';
let delete_asset_from_count_list = 'delete_asset_from_count_list';
let delete_count = 'delete_count';
let submit_count = 'submit_count';
let add_asset_to_count = 'add_asset_to_count_no';

// end of function name of api 




let getcostcenter = 'getcostcenter';
let createnewrequest = 'create_new_request';
let getitem = 'getitem';
let qtyissue = 'qtyissue';
let getstock = 'getstock';
let get_transaction = 'get_transaction';
let get_request_list_by_user = 'get_request_list_by_user';
let get_asset_list_by_req_num = 'get_asset_list_by_req_num';
let delete_asset_from_list = 'delete_asset_from_list';
let submit_request = 'submit_request';
let delete_request = 'delete_request';
let add_asset_to_request = 'add_asset_to_request';
let get_approve_list_by_req_num = 'get_approve_list_by_req_num';


// let apiUrl = 'http://localhost/inventory/api/mobile/login?username=admin@admin.com&password=admin';
/*
  Generated class for the AuthService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class AuthService {

  constructor(public http: Http) {
    console.log('Hello AuthService Provider');
  }

// list api of fix asset dhl ecommerce project 
// 1. login
postData(credentials){

  let username = credentials.username ;
  let password = credentials.password ;

  let var_username = '?username='+username;
  let var_password = '&password='+password;

   return new Promise((resolve, reject) =>{
     let headers = new Headers({
       // 'Content-Type': 'application/json;charset=UTF-8'
       'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
        });
      headers.append('Content-Type', 'application/json; charset=UTF-8');

     //let headers = { 'Content-Type': 'application/json;charset=UTF-8'};
   //  this.http.get(apiUrl, JSON.stringify(credentials), {headers: headers}).
   this.http.get(apiUrl+login+var_username+var_password,  {headers: headers}).
  //  this.http.post(apiUrl,credentials ,  {headers: headers}).
     //this.http.post(apiUrl, credentials, "{ 'Content-Type': 'application/json;charset=UTF-8'}").
     subscribe(res =>{
        resolve(res.json());
        console.log("Case 1");
       // resolve(res);
     }, (err) =>{
       reject(err);
       console.log("Case 2");
     });

   });

 }
// end of 1.login

// 2. create new count no
createNewCount(){
   
  const data = JSON.parse(localStorage.getItem("userData"));
  let center_code_string = JSON.stringify(data.cost_center_code);
  let center_code = center_code_string.substring(1, center_code_string.length - 1);

  let user_id_string = JSON.stringify(data.user_id);
  let user_id = user_id_string.substring(1, user_id_string.length - 1);

  console.log("send user id");
  console.log(user_id);
  
  let var_user_id = '?user_id='+user_id;
  let var_center_code = '&costcenter_code='+center_code;

  return new Promise((resolve, reject) =>{
    

    let headers = new Headers({
      // 'Content-Type': 'application/json;charset=UTF-8'
      'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
       });
     headers.append('Content-Type', 'application/json; charset=UTF-8');

        //alert(apiUrl+qtyissue+var_item_id+var_qty+var_center_code+var_username);
  
        this.http.get(apiUrl+create_new_count+var_user_id+var_center_code
          ,  {headers: headers}).

              subscribe(res =>{
                resolve(res.json());
              //  alert("Case 1");
              // alert(res.json());
              }, (err) =>{
                reject(err);
                // console.log("Case 2");
                alert(err);
                //  alert("Case 2");

              });

        });

}
// end of 2.create new count no

// 3. Get count list by user_id
get_count_list_by_use(){

  let data = JSON.parse(localStorage.getItem("userData"));
  // let username_string = JSON.stringify(data.name);
  // let username = username_string.substring(1, username_string.length - 1);

  // let whse_string = JSON.stringify(data.whse);
  // let center_code = whse_string.substring(1, whse_string.length - 1);

  let user_id_string = JSON.stringify(data.user_id);
  let user_id = user_id_string.substring(1, user_id_string.length - 1);
  let var_user_id = '?user_id='+user_id;

  // let var_username = '?username='+username;
  // let var_center_code = '&center_code='+center_code;

  return new Promise((resolve, reject) =>{
    

    let headers = new Headers({
      // 'Content-Type': 'application/json;charset=UTF-8'
      'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
       });
     headers.append('Content-Type', 'application/json; charset=UTF-8');
  
        this.http.get(apiUrl+get_count_list_by_user+var_user_id ,  {headers: headers}).

              subscribe(res =>{
                resolve(res.json());
                // alert("Case 1");
                // alert(res.json());
              }, (err) =>{
                reject(err);
                console.log("Case 2");
                alert(err);
                // alert("Case 2");

              });

        });

}
// end of 3. get count list by user id

// 4. get asset list by count no
get_asset_list_by_count_no(value){

  let var_count_no = '?count_no='+value.count_no;

  return new Promise((resolve, reject) =>{
  
    let headers = new Headers({
      // 'Content-Type': 'application/json;charset=UTF-8'
      'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
       });
     headers.append('Content-Type', 'application/json; charset=UTF-8');
  
        this.http.get(apiUrl+get_asset_list_by_count_no+var_count_no ,  {headers: headers}).

              subscribe(res =>{
                resolve(res.json());
                // alert("Case 1");
                // alert(res.json());
              }, (err) =>{
                reject(err);
                console.log("Case 2");
                alert(err);
                // alert("Case 2");

              });

        });

}
// end of get asset list by count no

// 5. get asset detail by asset_id
get_asset_detail(asset_id){
     
  let var_asset_id = '?asset_id='+asset_id;

// let var_username = '?username='+username;
// let var_center_code = '&center_code='+center_code;

return new Promise((resolve, reject) =>{
  

  let headers = new Headers({
    // 'Content-Type': 'application/json;charset=UTF-8'
    'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
     });
   headers.append('Content-Type', 'application/json; charset=UTF-8');

      this.http.get(apiUrl+get_asset_detail+var_asset_id ,  {headers: headers}).

            subscribe(res =>{
              resolve(res.json());
              // alert("Case 1");
              // alert(res.json());
            }, (err) =>{
              reject(err);
              console.log("Case 2");
              alert(err);
              // alert("Case 2");

            });

      });
}
// end of 5.get asset detail by asset_id

// 6. delete asset from count list
delete_asset_from_count_list(count_detail_id){


  let var_count_detail_id = '?count_detail_id='+count_detail_id;

  return new Promise((resolve, reject) =>{
    

    let headers = new Headers({
      // 'Content-Type': 'application/json;charset=UTF-8'
      'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
       });
     headers.append('Content-Type', 'application/json; charset=UTF-8');
  
        this.http.get(apiUrl+delete_asset_from_count_list+var_count_detail_id ,  {headers: headers}).

              subscribe(res =>{
                resolve(res.json());
                console.log(res.json());
                // alert("Case 1");
                // alert(res.json());
              }, (err) =>{
                reject(err);
                console.log("Case 2");
                // alert(err);
                // alert("Case 2");

              });
        });
}
// end of 6. delete asset from count list

// 7. delete count no
delete_count(count_no){

  let var_count_no = '?count_no='+count_no;

  return new Promise((resolve, reject) =>{
    

    let headers = new Headers({
      // 'Content-Type': 'application/json;charset=UTF-8'
      'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
       });
     headers.append('Content-Type', 'application/json; charset=UTF-8');
  
        this.http.get(apiUrl+delete_count+var_count_no ,  {headers: headers}).

              subscribe(res =>{
                resolve(res.json());
                console.log(res.json());
                // alert("Case 1");
                // alert(res.json());
              }, (err) =>{
                reject(err);
                console.log("Case 2");
                // alert(err);
                // alert("Case 2");

              });

        });
}
// end of delete count no

// 7. submit count no
submit_count(count_no){

  let var_count_no = '?count_no='+count_no;

  return new Promise((resolve, reject) =>{
    
    let headers = new Headers({
      // 'Content-Type': 'application/json;charset=UTF-8'
      'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
       });
     headers.append('Content-Type', 'application/json; charset=UTF-8');
  
        this.http.get(apiUrl+submit_count+var_count_no ,  {headers: headers}).

              subscribe(res =>{
                resolve(res.json());
                console.log(res.json());
                // alert("Case 1");
                // alert(res.json());
              }, (err) =>{
                reject(err);
                console.log("Case 2");
                // alert(err);
                // alert("Case 2");

              });

        });
}
// end of 7. submit count no


// 8. add asset to count document 
add_asset_to_count(qr_code_new,count_no){

  const data = JSON.parse(localStorage.getItem("userData"));

  let user_id_string = JSON.stringify(data.user_id);
  let user_id = user_id_string.substring(1, user_id_string.length - 1);

  let cost_center_code_string = JSON.stringify(data.cost_center_code);
  let cost_center_code = cost_center_code_string.substring(1, cost_center_code_string.length - 1);

  let var_user_id = '?user_id='+user_id;
  let var_cost_center_code = '&costcenter_code='+cost_center_code;
  let var_count_no = '&count_no='+count_no;
  let var_qr_code = '&qr_code='+qr_code_new;

  return new Promise((resolve, reject) =>{
    

    let headers = new Headers({
      // 'Content-Type': 'application/json;charset=UTF-8'
      'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
       });
     headers.append('Content-Type', 'application/json; charset=UTF-8');

        // alert(apiUrl+add_asset_to_request+var_user_id+var_req_number+var_qr_code);
  
        this.http.get(apiUrl+add_asset_to_count+var_user_id+var_count_no+var_cost_center_code+var_qr_code
          ,  {headers: headers}).

              subscribe(res =>{
                resolve(res.json());
              //  alert("Case 1");
              // alert(res.json());
              }, (err) =>{
                reject(err);
                // console.log("Case 2");
                alert(err);
                //  alert("Case 2");

              });

        });

}
// end of 8. add asset to count document

// end of list api of fix asset dhl ecommerce project
//////////////////////////////////////////////////////

  getCostcenter(){

    // let data = JSON.parse(localStorage.getItem("userData"));
    // let whse_string = JSON.stringify(data.whse);
    // let center_code = whse_string.substring(1, whse_string.length - 1);


    // let var_center_code = '?center_code='+center_code;

    return new Promise((resolve, reject) =>{
      

      let headers = new Headers({
        // 'Content-Type': 'application/json;charset=UTF-8'
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
         });
       headers.append('Content-Type', 'application/json; charset=UTF-8');

          //alert(apiUrl+qtyissue+var_item_id+var_qty+var_center_code+var_username);
          // alert(apiUrl+getstock+var_center_code);
          this.http.get(apiUrl+getcostcenter,  {headers: headers}).

                subscribe(res =>{
                  resolve(res.json());
                  // alert("Case 1");
                  // alert(res.json());
                }, (err) =>{
                  reject(err);
                  console.log("Case 2");
                  alert(err);
                  // alert("Case 2");

                });

          });

  }

  submit_request(req_number){

    let var_req_number = '?req_number='+req_number;

    return new Promise((resolve, reject) =>{
      

      let headers = new Headers({
        // 'Content-Type': 'application/json;charset=UTF-8'
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
         });
       headers.append('Content-Type', 'application/json; charset=UTF-8');
    
          this.http.get(apiUrl+submit_request+var_req_number ,  {headers: headers}).

                subscribe(res =>{
                  resolve(res.json());
                  console.log(res.json());
                  // alert("Case 1");
                  // alert(res.json());
                }, (err) =>{
                  reject(err);
                  console.log("Case 2");
                  // alert(err);
                  // alert("Case 2");

                });

          });
  }

  delete_request(req_number){

    let var_req_number = '?req_number='+req_number;

    return new Promise((resolve, reject) =>{
      

      let headers = new Headers({
        // 'Content-Type': 'application/json;charset=UTF-8'
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
         });
       headers.append('Content-Type', 'application/json; charset=UTF-8');
    
          this.http.get(apiUrl+delete_request+var_req_number ,  {headers: headers}).

                subscribe(res =>{
                  resolve(res.json());
                  console.log(res.json());
                  // alert("Case 1");
                  // alert(res.json());
                }, (err) =>{
                  reject(err);
                  console.log("Case 2");
                  // alert(err);
                  // alert("Case 2");

                });

          });
  }
 
  delete_asset_from_list(request_detail_id){


    let var_request_detail_id = '?count_detail_id='+request_detail_id;
  
    return new Promise((resolve, reject) =>{
      
  
      let headers = new Headers({
        // 'Content-Type': 'application/json;charset=UTF-8'
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
         });
       headers.append('Content-Type', 'application/json; charset=UTF-8');
    
          this.http.get(apiUrl+delete_asset_from_list+var_request_detail_id ,  {headers: headers}).
  
                subscribe(res =>{
                  resolve(res.json());
                  console.log(res.json());
                  // alert("Case 1");
                  // alert(res.json());
                }, (err) =>{
                  reject(err);
                  console.log("Case 2");
                  // alert(err);
                  // alert("Case 2");
  
                });
          });
  }

  createNewRequest(req){
   
    let request_type = req.request_type;
    let to_costcenter = req.to_costcenter;

    const data = JSON.parse(localStorage.getItem("userData"));
    let whse_string = JSON.stringify(data.whse);
    let center_code = whse_string.substring(1, whse_string.length - 1);

    let user_id_string = JSON.stringify(data.admin_id);
    let user_id = user_id_string.substring(1, user_id_string.length - 1);

    let username_string = JSON.stringify(data.name);
    let username = username_string.substring(1, username_string.length - 1);

    console.log("send user id");
    console.log(user_id);
    //alert(item_id);
    //alert(qty);
    //alert(center_code);
    //alert(username);

    let var_request_type = '?request_type='+request_type;
    let var_to_costcenter = '&to_costcenter='+to_costcenter;
    let var_center_code = '&center_code='+center_code;
    let var_username = '&username='+username;
    let var_user_id = '&user_id='+user_id;

    return new Promise((resolve, reject) =>{
      

      let headers = new Headers({
        // 'Content-Type': 'application/json;charset=UTF-8'
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
         });
       headers.append('Content-Type', 'application/json; charset=UTF-8');

          //alert(apiUrl+qtyissue+var_item_id+var_qty+var_center_code+var_username);
    
          this.http.get(apiUrl+createnewrequest+var_request_type+var_to_costcenter+var_center_code+var_username+var_user_id
            ,  {headers: headers}).

                subscribe(res =>{
                  resolve(res.json());
                //  alert("Case 1");
                // alert(res.json());
                }, (err) =>{
                  reject(err);
                  // console.log("Case 2");
                  alert(err);
                  //  alert("Case 2");

                });

          });

  }

  add_asset_to_request(qr_code,req_number){

    const data = JSON.parse(localStorage.getItem("userData"));

    let user_id_string = JSON.stringify(data.admin_id);
    let user_id = user_id_string.substring(1, user_id_string.length - 1);

    let var_user_id = '?user_id='+user_id;
    let var_req_number = '&req_number='+req_number;
    let var_qr_code = '&qr_code='+qr_code;

    return new Promise((resolve, reject) =>{
      

      let headers = new Headers({
        // 'Content-Type': 'application/json;charset=UTF-8'
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
         });
       headers.append('Content-Type', 'application/json; charset=UTF-8');

          // alert(apiUrl+add_asset_to_request+var_user_id+var_req_number+var_qr_code);
    
          this.http.get(apiUrl+add_asset_to_request+var_user_id+var_req_number+var_qr_code
            ,  {headers: headers}).

                subscribe(res =>{
                  resolve(res.json());
                //  alert("Case 1");
                // alert(res.json());
                }, (err) =>{
                  reject(err);
                  // console.log("Case 2");
                  alert(err);
                  //  alert("Case 2");

                });

          });

  }

  insertAssetList(qrtext){
      alert(qrtext.req_num);
      alert(qrtext.qr_code);
  }


  getItemDetail(item_barcode){

    // alert("getItemDetail function");

    let barcode_text = item_barcode;
    let data = JSON.parse(localStorage.getItem("userData"));
    let whse_string = JSON.stringify(data.whse);
    let whse = whse_string.substring(1, whse_string.length - 1);
    //JSON.stringify(data.whse);
    // alert(whse);


    let var_barcode = '?barcode_text='+barcode_text;
    let var_whse = '&whse='+whse;

    return new Promise((resolve, reject) =>{
      

      let headers = new Headers({
        // 'Content-Type': 'application/json;charset=UTF-8'
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
         });
       headers.append('Content-Type', 'application/json; charset=UTF-8');
         //alert(apiUrl+getitem+var_barcode+var_whse);
    
          this.http.get(apiUrl+getitem+var_barcode+var_whse,  {headers: headers}).
        //  this.http.post(apiUrl,credentials ,  {headers: headers}).
            //this.http.post(apiUrl, credentials, "{ 'Content-Type': 'application/json;charset=UTF-8'}").
            subscribe(res =>{
              resolve(res.json());
             // alert("Case 1");
             // alert(res.json());
             }, (err) =>{
              reject(err);
              // console.log("Case 2");
              alert(err);
            });

          });

   }


  postQtyIssue(itemDetail){
    let qty = itemDetail.qty_issue;
    let item_id = itemDetail.item_id;

    let note = itemDetail.note;
    let ref_doc = itemDetail.ref_doc;

    let data = JSON.parse(localStorage.getItem("userData"));
    let whse_string = JSON.stringify(data.whse);
    let center_code = whse_string.substring(1, whse_string.length - 1);

    let username_string = JSON.stringify(data.name);
    let username = username_string.substring(1, username_string.length - 1);

    //alert(item_id);
    //alert(qty);
    //alert(center_code);
    //alert(username);

    let var_item_id = '?item_id='+item_id;
    let var_qty = '&qty='+qty;
    let var_center_code = '&center_code='+center_code;
    let var_username = '&username='+username;

    let var_note = '&note='+note;
    let var_ref_doc = '&ref_doc='+ref_doc;

    return new Promise((resolve, reject) =>{
      

      let headers = new Headers({
        // 'Content-Type': 'application/json;charset=UTF-8'
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
         });
       headers.append('Content-Type', 'application/json; charset=UTF-8');

          //alert(apiUrl+qtyissue+var_item_id+var_qty+var_center_code+var_username);
    
          this.http.get(apiUrl+qtyissue+var_item_id+var_qty+var_center_code+var_username+var_note+var_ref_doc
            ,  {headers: headers}).

                subscribe(res =>{
                  resolve(res.json());
                //  alert("Case 1");
                // alert(res.json());
                }, (err) =>{
                  reject(err);
                  // console.log("Case 2");
                  alert(err);
                  //  alert("Case 2");

                });

          });

  }

  getStock(){

    let data = JSON.parse(localStorage.getItem("userData"));
    let whse_string = JSON.stringify(data.whse);
    let center_code = whse_string.substring(1, whse_string.length - 1);


    let var_center_code = '?center_code='+center_code;

    return new Promise((resolve, reject) =>{
      

      let headers = new Headers({
        // 'Content-Type': 'application/json;charset=UTF-8'
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
         });
       headers.append('Content-Type', 'application/json; charset=UTF-8');

          //alert(apiUrl+qtyissue+var_item_id+var_qty+var_center_code+var_username);
          // alert(apiUrl+getstock+var_center_code);
          this.http.get(apiUrl+getstock+var_center_code,  {headers: headers}).

                subscribe(res =>{
                  resolve(res.json());
                  // alert("Case 1");
                  // alert(res.json());
                }, (err) =>{
                  reject(err);
                  console.log("Case 2");
                  alert(err);
                  // alert("Case 2");

                });

          });

  }


  get_request_list_by_use(){

    let data = JSON.parse(localStorage.getItem("userData"));
    // let username_string = JSON.stringify(data.name);
    // let username = username_string.substring(1, username_string.length - 1);

    // let whse_string = JSON.stringify(data.whse);
    // let center_code = whse_string.substring(1, whse_string.length - 1);

    let user_id_string = JSON.stringify(data.admin_id);
    let user_id = user_id_string.substring(1, user_id_string.length - 1);
    let var_user_id = '?user_id='+user_id;

    // let var_username = '?username='+username;
    // let var_center_code = '&center_code='+center_code;

    return new Promise((resolve, reject) =>{
      

      let headers = new Headers({
        // 'Content-Type': 'application/json;charset=UTF-8'
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
         });
       headers.append('Content-Type', 'application/json; charset=UTF-8');
    
          this.http.get(apiUrl+get_request_list_by_user+var_user_id ,  {headers: headers}).

                subscribe(res =>{
                  resolve(res.json());
                  // alert("Case 1");
                  // alert(res.json());
                }, (err) =>{
                  reject(err);
                  console.log("Case 2");
                  alert(err);
                  // alert("Case 2");

                });

          });

  }




  get_asset_list_by_req_num(value){


    let var_req_num = '?req_num='+value.req_number;

    return new Promise((resolve, reject) =>{
      

      let headers = new Headers({
        // 'Content-Type': 'application/json;charset=UTF-8'
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
         });
       headers.append('Content-Type', 'application/json; charset=UTF-8');
    
          this.http.get(apiUrl+get_asset_list_by_req_num+var_req_num ,  {headers: headers}).

                subscribe(res =>{
                  resolve(res.json());
                  // alert("Case 1");
                  // alert(res.json());
                }, (err) =>{
                  reject(err);
                  console.log("Case 2");
                  alert(err);
                  // alert("Case 2");

                });

          });

  }

  get_approve_list_by_req_num(value){
    let var_req_num = '?req_num='+value.req_number;

    return new Promise((resolve, reject) =>{
      

      let headers = new Headers({
        // 'Content-Type': 'application/json;charset=UTF-8'
        'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
         });
       headers.append('Content-Type', 'application/json; charset=UTF-8');
    
          this.http.get(apiUrl+get_approve_list_by_req_num+var_req_num ,  {headers: headers}).

                subscribe(res =>{
                  resolve(res.json());
                  // alert("Case 1");
                  // alert(res.json());
                }, (err) =>{
                  reject(err);
                  console.log("Case 2");
                  alert(err);
                  // alert("Case 2");

                });

          });
  }


  
// end of get_transaction function


  // start get_transaction function 

  get_transaction(){

        let data = JSON.parse(localStorage.getItem("userData"));
        let username_string = JSON.stringify(data.name);
        let username = username_string.substring(1, username_string.length - 1);

        let whse_string = JSON.stringify(data.whse);
        let center_code = whse_string.substring(1, whse_string.length - 1);

        let var_username = '?username='+username;
        let var_center_code = '&center_code='+center_code;

        return new Promise((resolve, reject) =>{
          

          let headers = new Headers({
            // 'Content-Type': 'application/json;charset=UTF-8'
            'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
            });
          headers.append('Content-Type', 'application/json; charset=UTF-8');

              //alert(apiUrl+qtyissue+var_item_id+var_qty+var_center_code+var_username);
              // alert(apiUrl+getstock+var_center_code);
              this.http.get(apiUrl+get_transaction+var_username+var_center_code,  {headers: headers}).

                    subscribe(res =>{
                      resolve(res.json());
                      // alert("Case 1");
                      // alert(res.json());
                    }, (err) =>{
                      reject(err);
                      console.log("Case 2");
                      alert(err);
                      // alert("Case 2");

                    });

              });

      }
  // end of get_transaction function

}
