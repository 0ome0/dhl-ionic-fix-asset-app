import { CountPage } from './../count/count';

import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { HomePage } from '../home/home';
import { ContactPage } from '../contact/contact';
import { CountListPage } from './../count-list/count-list';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

 
  tab1Root = CountPage;
  tab2Root = CountListPage;
  // tab3Root = HomePage;
  // tab4Root = AboutPage;

  constructor() {

  }
}
