import { CountListPage } from './../count-list/count-list';
import { Welcome } from './../welcome/welcome';
import { Component } from '@angular/core';
import { IonicPage, ToastController,  NavController, NavParams , App } from 'ionic-angular';
import { AuthService } from "../../providers/auth-service";
import { Common } from "../../providers/common";
import { BarcodeScanner } from '@ionic-native/barcode-scanner';



/**
 * Generated class for the CountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-count',
  templateUrl: 'count.html',
})
export class CountPage {

  public resposeData: any;

  userPostData = {
    user_id: "",
    user: "",
    email : "",
    cost_center_code : "",
    cost_description : "",
    // whse: "",
    // token: "",
    // feed: "",
    // feed_id: "",
    // lastCreated: ""
  };

  show_create_count : boolean;
  show_img_icon : boolean;

  

  constructor(public navCtrl: NavController,
      public navParams: NavParams,
      public common: Common,
      public app: App,
      public authService: AuthService,
      public barcodeScanner: BarcodeScanner,
      private toastCtrl:ToastController
     ) {

    const data = JSON.parse(localStorage.getItem("userData"));
    console.log(JSON.stringify(data.user_id));
    // this.userDetails = data.userData;
     this.userPostData.user_id = JSON.stringify(data.user_id);
     this.userPostData.user = JSON.stringify(data.user);
     this.userPostData.email = JSON.stringify(data.email);
     this.userPostData.cost_center_code = JSON.stringify(data.cost_center_code);
     this.userPostData.cost_description = JSON.stringify(data.cost_description);
    //  this.userPostData.whse = JSON.stringify(data.whse);

    this.show_create_count = false;
    this.show_img_icon = true ;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CountPage');
  }

  create_count_doc(){
    alert("create_count_doc");
  }

  send_create_count(){
    // alert("send_create_count");

    if(this.userPostData.user_id){
      this.common.presentLoading();

      this.authService.createNewCount().then((result) =>{
          this.resposeData = result;
          this.common.closeLoading();
          let code_string = JSON.stringify(this.resposeData.code);
          let code =  code_string.substring(1, code_string.length - 1);
          let msg = JSON.stringify(this.resposeData.msg);

          //  alert(this.resposeData);
          //  alert(code);
          
              if(code === "Success"){
              
              // this.notification.beep(1);
              // this.common.closeLoading();
              alert('Result : '+code+' - '+msg);
              

              // this.reset_button();
              // this.req.request_type =  "";
              
              this.common.presentLoading();

              if (this.resposeData.length) {
                this.common.closeLoading();
                    
                let i : any;
                  for (i = 0; i < this.resposeData.length; i++) {
                      console.log(this.resposeData.length);
                      console.log(this.resposeData);
                    // console.log(this.resposeData[i].costcenter_description);
                    
                }
                this.navCtrl.push(CountListPage);

              } else {
                console.log("No access");
                this.common.closeLoading();
                this.navCtrl.push(CountListPage);
                // alert("No Costcenter");
              }

                    
               }
               else{
                        this.common.closeLoading();
                        alert("ไม่สามารถ สร้างรายการตรวจนับได้ กรุณา ตรวจสอบสถานะของรายการตรวจนับ");
                        console.log("error");

                    }
                    
                  }, (err) => {
                    //Connection failed message
                    this.common.closeLoading();
                    alert("ไม่สามารถ สร้างรายการตรวจนับได้ กรุณา ตรวจสอบสถานะของรายการตรวจนับ");
                    }); // end of err
                }
  }

  reset_button(){

  }

logout() {
    //Api Token Logout
    // alert("TESTTSET");
    localStorage.clear();
    setTimeout(() => this.backToWelcome(), 100);
  }

  backToWelcome() {
    console.log("logout");
    // alert("logout");

    // const root = this.app.getRootNav().setRoot(Welcome);
    this.navCtrl.parent.parent.setRoot(Welcome);
    
    // const root = this.app.getRootNavById();

    // const root = this.app.getRootNav (); // in this line, you have to declare a root, which is the app's root 
    // root.popToRoot (); // here you go to the root.
  
    // const root = this.app.getRootNavs();
    // console.log(root);
    // root.popToRoot();
    // this.navCtrl.setRoot(Welcome);
    
    // // this.navCtrl.push(TabsPage);
    // this.navCtrl.push(Welcome); 
    // this.navCtrl.popAll();
    // this.navCtrl.pop()  
  }

}
