import { RequestPage } from './../request/request';
import { Component , ViewChild} from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import { AuthService } from "../../providers/auth-service";
import { Common } from "../../providers/common";

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})

export class AboutPage {

  whse : string;
  username : string ;

  public viewLoaded : boolean = false;
  
  @ViewChild("updatebox") updatebox;

  public resposeData: any;
  public dataSet: any;
  public noRecords: boolean;

  

  constructor(public navCtrl: NavController ,
    public navParams: NavParams,
    public common: Common,
    public authService: AuthService
  ) {
    const data = JSON.parse(localStorage.getItem("userData"));
    this.whse = JSON.stringify(data.whse);

    let username_string = JSON.stringify(data.name);
    this.username = username_string.substring(1, username_string.length - 1);


    this.getUserRequestList();

  }

  // start function
  getUserRequestList(){
      // alert("Check Function");
      this.common.presentLoading();
      this.authService.get_request_list_by_use().then((result) =>{
        this.resposeData = result;

           //console.log(this.resposeData.length);
           console.log(this.resposeData);
            if (this.resposeData.length) {
              this.common.closeLoading();
                  
              let i : any;
                for (i = 0; i < this.resposeData.length; i++) {
                  // console.log(this.resposeData[i].item_description);
              }

            } else {
              console.log("No access");
              this.common.closeLoading();
              alert("No User Transaction");
            }
              
      }, 
      (err) => {
        //Connection failed message
        alert("Please check your internet connection");
        }); // end of err

        
        
  }
  // end function

  viewRequestDetail(item){
    // console.log(item);
    this.viewLoaded = true;
    this.navCtrl.push(RequestPage,{item});
  
  }

  ionViewDidEnter(){
   
    console.log(this.viewLoaded);

    if(this.viewLoaded){
      this.getUserRequestList();
    } else {
      console.log("No Action");
    }

    
  }

}
