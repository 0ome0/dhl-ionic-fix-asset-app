import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';

import { AuthService } from "../../providers/auth-service";
import { Common } from "../../providers/common";

/**
 * Generated class for the AssetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-asset',
  templateUrl: 'asset.html',
})
export class AssetPage {
  value:any;
  public resposeData: any;

  constructor(public navCtrl: NavController
            , public authService: AuthService
            , public navParams: NavParams
            , public common: Common 
            ) {

              this.value = navParams.get('assets');
              // console.log(this.value);
              this.get_asset_detail();
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad AssetPage');
      
  }

  get_asset_detail(){

    console.log(this.value.asset_id);
    
    this.common.presentLoading();
    this.authService.get_asset_detail(this.value.asset_id).then((result) =>{
      this.resposeData = result;

         //console.log(this.resposeData.length);
         console.log(this.resposeData);
          if (this.resposeData.length) {
            this.common.closeLoading();
                
            let i : any;
              for (i = 0; i < this.resposeData.length; i++) {
                console.log("return data");
                console.log(this.resposeData[i]);
                // alert("Result from API - Asset Detail");
            }

          } else {
            console.log("No record");
            this.common.closeLoading();
            alert("No asset in list");

          }
            
    }, 
    (err) => {
      //Connection failed message
      alert("Please check your internet connection");
      }); // end of err
  }


}
