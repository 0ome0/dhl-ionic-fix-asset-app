import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';

import { AuthService } from "../../providers/auth-service";
import { Common } from "../../providers/common";


/**
 * Generated class for the ApprovePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-approve',
  templateUrl: 'approve.html',
})
export class ApprovePage {

  value:any;
  public resposeData: any;

  constructor(
    public navCtrl: NavController
    , public authService: AuthService
    , public navParams: NavParams
    , public common: Common 
  ) {

    this.value = navParams.get('request');
    // console.log(this.value);
    this.get_approve_list();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApprovePage');
  }

  get_approve_list(){

    console.log(this.value.req_number);
    
    this.common.presentLoading();
    this.authService.get_approve_list_by_req_num(this.value).then((result) =>{
      this.resposeData = result;

         //console.log(this.resposeData.length);
         console.log(this.resposeData);
          if (this.resposeData.length) {
            this.common.closeLoading();
                
            let i : any;
              for (i = 0; i < this.resposeData.length; i++) {
                console.log("return data");
                console.log(this.resposeData[i]);
                // alert("Result from API - Asset Detail");
            }

          } else {
            console.log("No record");
            this.common.closeLoading();
            alert("No approve transaction");

          }
            
    }, 
    (err) => {
      //Connection failed message
      alert("Please check your internet connection");
      }); // end of err
  }

}
