import { TabsPage } from './../tabs/tabs';
import { CountDetailPage } from './../count-detail/count-detail';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ViewChild} from '@angular/core';
import { AuthService } from "../../providers/auth-service";
import { Common } from "../../providers/common";
/**
 * Generated class for the CountListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-count-list',
  templateUrl: 'count-list.html',
})
export class CountListPage {

  user : string ;

  public viewLoaded : boolean = false;
  
  @ViewChild("updatebox") updatebox;

  public resposeData: any;
  public dataSet: any;
  public noRecords: boolean;

  constructor(public navCtrl: NavController ,
    public navParams: NavParams,
    public common: Common,
    public authService: AuthService
    ) {

      const data = JSON.parse(localStorage.getItem("userData"));
      // this.whse = JSON.stringify(data.whse);
  
      let user_string = JSON.stringify(data.user);
      this.user = user_string.substring(1, user_string.length - 1);

      this.getUserCountList();
  }


  // start function
  getUserCountList(){
    // alert("Check Function");
    this.common.presentLoading();
    this.authService.get_count_list_by_use().then((result) =>{
      this.resposeData = result;

         //console.log(this.resposeData.length);
         console.log(this.resposeData);
          if (this.resposeData.length) {
                  // this.common.closeLoading();                
                  let i : any;
                    for (i = 0; i < this.resposeData.length; i++) {
                      // console.log(this.resposeData[i].item_description);
                  }

          } else {
            console.log("No Count Transaction");
            // this.common.closeLoading();
            alert("ไม่มีรายการตรวจนับทรัพย์สิน");
          }
            
    }, 
    (err) => {
      //Connection failed message
      alert("ไม่มีรายการตรวจนับทรัพย์สิน");
      // this.common.closeLoading();
      }); // end of err
      this.common.closeLoading();
}
// end function

viewCountDetail(item){
  // console.log(item);
  this.viewLoaded = true;
  this.navCtrl.push(CountDetailPage,{item});

}

  ionViewDidLoad() {
    console.log('ionViewDidLoad CountListPage');
  }

  ionViewDidEnter(){
   
    
    console.log(this.viewLoaded);

    console.log("Count List Page did enter");
    // this.getUserCountList();

    if(this.viewLoaded){
      
      this.getUserCountList();
      
    } else {
      console.log("No Action");
    }
    
  }

}
