import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CountListPage } from './count-list';

@NgModule({
  declarations: [
    CountListPage,
  ],
  imports: [
    IonicPageModule.forChild(CountListPage),
  ],
})
export class CountListPageModule {}
