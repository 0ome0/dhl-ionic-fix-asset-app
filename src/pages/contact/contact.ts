import { Component , ViewChild} from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthService } from "../../providers/auth-service";
import { Common } from "../../providers/common";
import { TabsPage } from '../tabs/tabs';
import { HomePage } from '../home/home';
import { AboutPage } from '../about/about';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  whse : string;
  
  itemDetail = {"item_id":"","item_code":"", "item_description":"","shelf_location":"", "qty_on_hand":"" , "qty_issue":"", "note":"", "ref_doc":""};

  @ViewChild("updatebox") updatebox;
 
  public resposeData: any;
  public dataSet: any;
  public noRecords: boolean;


  constructor(public navCtrl: NavController ,
    public common: Common,
    public authService: AuthService
  ) {
    const data = JSON.parse(localStorage.getItem("userData"));
    this.whse = JSON.stringify(data.whse);
    this.getStockOnhand();
  }

  getStockOnhand(){
    // alert("Check Function");
    this.common.presentLoading();
    this.authService.getStock().then((result) =>{
      this.resposeData = result;

      // console.log(this.resposeData[0].id);

      // let i : any;
      // for (i = 0; i < this.resposeData.length; i++) {
      //   // console.log(this.resposeData[i].item_description);
      //   this.item_detail.item_id =  this.resposeData[i].item_id ;
      //   this.item_detail.item_code =  this.resposeData[i].item_code ;
      //   this.item_detail.item_description =  this.resposeData[i].item_description ;

      // }
     
    // let x : any;

    // for (x = 0; x < this.resposeData.length; x++) {
    //  console.log(this.item_detail.item_description);    
    // }
       
        // console.log(this.resposeData.length);
          if (this.resposeData.length) {
            this.common.closeLoading();
                
            let i : any;
              for (i = 0; i < this.resposeData.length; i++) {

                console.log(this.resposeData[i].item_description);
            }

           } else {
            console.log("No access");
            this.common.closeLoading();
            alert("No Item QTY > 0");
          }
            
    }, 
    (err) => {
      //Connection failed message
      alert("Please check your internet connection");
      }); // end of err
  }

// testpage(){
//   // this.navCtrl.push(TabsPage.tab2root);  
//      this.navCtrl.push(TabsPage,AboutPage);
//   }
}
