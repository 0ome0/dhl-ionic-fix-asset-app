import { CountListPage } from './../count-list/count-list';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , ToastController} from 'ionic-angular';

import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { AuthService } from "../../providers/auth-service";
import { Common } from "../../providers/common";
import { AssetPage } from '../asset/asset';
import { ApprovePage } from '../approve/approve';


/**
 * Generated class for the CountDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-count-detail',
  templateUrl: 'count-detail.html',
})
export class CountDetailPage {

  value:any;
  qr_code : string;

  qrtext = {"req_num":"", "qrcode":""};

  public resposeData: any;
  public returnData : any;
  public dataSet: any;
  public noRecords: boolean;
  public qrscan_button : boolean;
  public submit_button : boolean;
  public view_button : boolean;

  constructor( 
    public common: Common,public authService: AuthService,
    public toastCtrl:ToastController,
    public navCtrl: NavController, public navParams: NavParams,private qrScanner: QRScanner) {

    this.value = navParams.get('item');
    console.log(this.value);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CountDetailPage');
    // window.document.querySelector('ion-app').classList.add('cameraView');
    console.log(this.value.submit_status);
   
    this.check_submit_status();
 
    this.showAssetList();
  }

  check_submit_status (){
     
    if(this.value.submit_status==='1'){
      this.submit_button = true; 
      this.qrscan_button = true;
      this.view_button  = false;
    } else {
      this.submit_button = false;
      this.qrscan_button = false;
      this.view_button  = true;
    }

  }

  qrScan(){
    console.log("Open Camera and send to save");
    // this.qrScanner.show()
    
    // scan
      // Optionally request the permission early
      this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          // camera permission was granted
          alert('ใช้หน้าจอมือถือ Scan ที่ QRCode ที่ทรัพย์สินที่ต้องการตรวจนับ');

          // this.qrScanner.show();

          // start scanning

          this.qrScanner.show()
          window.document.querySelector('ion-app').classList.add('cameraView');

          let scanSub = this.qrScanner.scan().subscribe((text: string) => {

            console.log('Scanned something', text);
  
            window.document.querySelector('ion-app').classList.remove('cameraView');
            this.qrScanner.hide(); // hide camera preview
            
            
            // let qr_code = text;
            this.addToCount(text);
           
            scanSub.unsubscribe(); // stop scanning
          });
        } else if (status.denied) {
          // camera permission was permanently denied
          // you must use QRScanner.openSettings() method to guide the user to the settings page
          // then they can grant the permission from there
          alert('denied');
        } else {
          // permission was denied, but not permanently. You can ask for permission again at a later time.
          alert('else');
        }
      })
      // .catch((e: any) => console.log('Error is', e)
      .catch((e: any) => alert(e)
      );
      
    // end of scan

  }

  addToCount(text:string){
    // alert(text);

        let qr_code : string = text ;
            qr_code = qr_code.substr(text.length - 15);

        let qr_code_new : string = text ;
            qr_code_new = qr_code.replace("=","");

        let count_no = this.value.count_no;
    
        alert("QRCode is "+qr_code_new+" for count no."+this.value.count_no);

        // add asset that scaned to request num 

        // this.authService.add_asset_to_request(qr_code,req_number).then((result) =>{

            this.common.presentLoading();
    
            this.authService.add_asset_to_count(qr_code_new,count_no).then((result) =>{
              this.returnData = result;
              // alert(this.returnData);
              
              alert(this.returnData.msg);
              this.showAssetList();
              this.check_submit_status();
                    
            }, 
            (err) => {
              //Connection failed message
              alert("ไม่สามารถ เพิ่มรายการทรัพย์สินได้");
              }); // end of err

              this.common.closeLoading();
            // end of function

          
            this.showAssetList();
        // end of function
  
  }


  showAssetList(){

    console.log(this.value.count_no);
    
    this.common.presentLoading();
    this.authService.get_asset_list_by_count_no(this.value).then((result) =>{
      this.resposeData = result;

         //console.log(this.resposeData.length);
         console.log(this.resposeData);
          if (this.resposeData.length) {
            // this.common.closeLoading();
                
            let i : any;
              for (i = 0; i < this.resposeData.length; i++) {
                // console.log(this.resposeData[i].item_description);
                // alert("Result from API - Asset List");
            }

            this.check_submit_status();

          } else {
            console.log("No record");
            // this.common.closeLoading();
            alert("ไม่มีรายการทรัพย์สินใน list");
            this.qrscan_button = true;
            this.submit_button = false; // test delete count
            this.view_button = false;
          }
          // this.common.closeLoading();
    }, 
    (err) => {
      //Connection failed message
      alert("ไม่สามารถ ลบรายการได้");
      // this.common.closeLoading();
      }); // end of err
      this.common.closeLoading();
  }

  viewAsset(assets:any){
    console.log({assets});
    // let request_detail_id = assets.request_detail_id;

    this.navCtrl.push(AssetPage,{assets});
  }

  removeAsset(assets:any){
    console.log(assets.count_detail_id);

    let count_detail_id = assets.count_detail_id;

    // send request_detail_id to api remove from list
    // this.authService.delete_asset_from_list(request_detail_id).then((result)
    this.common.presentLoading();
    
    this.authService.delete_asset_from_count_list(count_detail_id).then((result) =>{
      this.returnData = result;
      // alert(this.returnData);
      
      alert(this.returnData.msg);
      this.showAssetList();
             
    }, 
    (err) => {
      //Connection failed message
      alert("ไม่สามารถ ลบรายการได้");
      }); // end of err

      this.common.closeLoading();
    // end of function

  
    this.showAssetList();
  }

  viewApprove(){
    
    // let req_number_check = this.value.req_number;
    // console.log(this.value);
    let request : any = this.value;
    
    this.navCtrl.push(ApprovePage,{request});

  }

  deleteCount(){
    console.log("delete "+this.value.count_no);

      // send req_number to api update status = submit
          // this.authService.delete_asset_from_list(request_detail_id).then((result)
          this.common.presentLoading();
          
          this.authService.delete_count(this.value.count_no).then((result) =>{
            this.returnData = result;
            // this.common.closeLoading();
            alert(this.returnData.msg);
            let returnValue = true;
                    
          }, 
          (err) => {
            //Connection failed message
            // this.common.closeLoading();
            alert("ไม่สามารถ ลบรายการได้");

          }); // end of err

            this.common.closeLoading();
            this.navCtrl.pop();
          // end of function
        
  

  }

  submitCount(){
    console.log(this.value.count_no);

    
          // send req_number to api update status = submit
          // this.authService.delete_asset_from_list(request_detail_id).then((result)
          this.common.presentLoading();
          
          this.authService.submit_count(this.value.count_no).then((result) =>{
            this.returnData = result;
            // alert(this.returnData);
            alert(this.returnData.msg);
            let returnValue = true;
            this.navCtrl.pop();
                  
          }, 
          (err) => {
            //Connection failed message
            alert("ไม่สามารถ submit รายการตรวจนับได้");
            }); // end of err

            this.common.closeLoading();
          // end of function

        
          this.showAssetList();
  }
}
