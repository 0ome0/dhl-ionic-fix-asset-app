import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CountDetailPage } from './count-detail';

@NgModule({
  declarations: [
    CountDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(CountDetailPage),
  ],
})
export class CountDetailPageModule {}
