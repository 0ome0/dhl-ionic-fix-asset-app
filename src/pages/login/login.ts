import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import {AuthService} from "../../providers/auth-service";
import { Common } from "../../providers/common";
/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {
  
  resposeData : any;
  userData = {"username":"", "password":""};

  constructor(public navCtrl: NavController, public authService: AuthService, private toastCtrl:ToastController
    ,public common: Common
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  login(){

    // this.userData.username = "admin@admin.com";
    // this.userData.password = "admin";
      this.common.presentLoading();
      if(this.userData.username && this.userData.password){
          this.authService.postData(this.userData).then((result) =>{
              this.resposeData = result;
              // console.log(this.resposeData);

                let user_id = JSON.stringify(this.resposeData.user_id);
                let user = JSON.stringify(this.resposeData.user);
                let email = JSON.stringify(this.resposeData.email);
                let cost_center_code = JSON.stringify(this.resposeData.cost_center_code);
                let cost_description = JSON.stringify(this.resposeData.cost_description);

                console.log(this.resposeData);
                // if(this.resposeData.userData){
                if(name!=="null"){
                // console.log(JSON.stringify(this.resposeData.admin_id));
                this.common.closeLoading();
                localStorage.setItem('userData', JSON.stringify(this.resposeData) )
                this.presentToast("Welcome "+user+" "+cost_description);
                
                this.navCtrl.push(TabsPage);
                console.log("Return Have value");
                // console.log(name);
                // let data = JSON.parse(localStorage.getItem("userData"));
                // console.log('show local storage');
                // console.log(data);
                }
                else{
                  console.log("Return Nooo value");
                  this.common.closeLoading();
                  this.presentToast("Please give valid username and password");
                }
              
            }, (err) => {
              //Connection failed message
              this.common.closeLoading();
              }); // end of err
      }

      // check must have input username and password
      else{ 
        this.presentToast("Pleas input username and password to login");
        this.common.closeLoading();
      }
  
  }


  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
