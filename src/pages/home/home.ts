import { Component, ViewChild } from "@angular/core";
import { NavController, App, AlertController } from "ionic-angular";
import { AuthService } from "../../providers/auth-service";
import { Common } from "../../providers/common";
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { IonicPage, ToastController } from 'ionic-angular';
import { Welcome } from "../welcome/welcome";
import { TabsPage } from "../tabs/tabs";
import { AboutPage } from "../about/about";


@Component({ selector: "page-home", templateUrl: "home.html" })
export class HomePage {

  itemDetail = {"item_id":"","item_code":"", "item_description":"","shelf_location":"", "qty_on_hand":"" , "qty_issue":"", "note":"", "ref_doc":""};

  req = { "request_type":"",
          "to_costcenter":[]};


  item_barcode : string;
  show_result : boolean;
  show_scanButton : boolean;
  show_waybill : boolean;
  show_submit : boolean;
  item_description : string ; 

  show_create_request : boolean;
  show_img_icon : boolean;


  @ViewChild("updatebox") updatebox;
  public userDetails: any;
  public resposeData: any;
  public dataSet: any;
  public noRecords: boolean;
  userPostData = {
    user_id: "",
    user_name: "",
    email : "",
    firstname : "",
    lastname : "",
    whse: "",
    // token: "",
    // feed: "",
    // feed_id: "",
    // lastCreated: ""
  };

  constructor(
    public common: Common,
    private alertCtrl: AlertController,
    public navCtrl: NavController,
    public app: App,
    public authService: AuthService,
    public barcodeScanner: BarcodeScanner,
    private toastCtrl:ToastController
  ) {
    const data = JSON.parse(localStorage.getItem("userData"));
    console.log(JSON.stringify(data.admin_id));
    // this.userDetails = data.userData;
     this.userPostData.user_id = JSON.stringify(data.admin_id);
     this.userPostData.user_name = JSON.stringify(data.name);
     this.userPostData.email = JSON.stringify(data.email);
     this.userPostData.firstname = JSON.stringify(data.firstname);
     this.userPostData.lastname = JSON.stringify(data.lastname);
     this.userPostData.whse = JSON.stringify(data.whse);
    // this.userPostData.token = this.userDetails.token;
    // this.userPostData.lastCreated = "";
    this.noRecords = false
    // this.getFeed();

    this.show_result = false;
    this.show_scanButton = true;
    this.show_waybill = false;
    this.show_submit = false;

    this.show_create_request = false;
    this.show_img_icon = true ;

  }
 

  create_request(){
    this.show_img_icon = false ;
    this.show_create_request = true;

    //load CostCenter
    this.common.presentLoading();
    this.authService.getCostcenter().then((result) =>{
      this.resposeData = result;
      // this.common.closeLoading();
        // console.log(this.resposeData.length);
          if (this.resposeData.length) {
            this.common.closeLoading();
                
            let i : any;
              for (i = 0; i < this.resposeData.length; i++) {
                   console.log(this.resposeData.length);
                // console.log(this.resposeData[i].costcenter_description);
            }

           } else {
            console.log("No access");
            this.common.closeLoading();
            alert("No Costcenter");
          }
            
    }, 
    (err) => {
      //Connection failed message
      alert("Please check your internet connection");
      }); // end of err
      // end of load costcenter
  }

  send_create_request(){

    if(this.req.request_type){
      this.common.presentLoading();

      this.authService.createNewRequest(this.req).then((result) =>{
          this.resposeData = result;
          this.common.closeLoading();
          let code_string = JSON.stringify(this.resposeData.code);
          let code =  code_string.substring(1, code_string.length - 1);
          let msg = JSON.stringify(this.resposeData.msg);

          //  alert(this.resposeData);
          //  alert(code);
          
              if(code === "Success"){
              
              // this.notification.beep(1);
              // this.common.closeLoading();
              alert('Result : '+code+' - '+msg);
              

              this.reset_button();
              this.req.request_type =  "";
              
              this.common.presentLoading();

              this.authService.getCostcenter().then((result) =>{
                this.resposeData = result;
                // this.common.closeLoading();
                  // console.log(this.resposeData.length);
                    if (this.resposeData.length) {
                      this.common.closeLoading();
                          
                      let i : any;
                        for (i = 0; i < this.resposeData.length; i++) {
                            // console.log(this.resposeData.length);
                          // console.log(this.resposeData[i].costcenter_description);
                          
                      }
                      this.navCtrl.push(AboutPage);

                    } else {
                      console.log("No access");
                      this.common.closeLoading();
                      alert("No Costcenter");
                    }
                      
              }, 
              (err) => {
                //Connection failed message
                alert("Please check your internet connection");
                }); // end of err
                // end of load costcenter
                          
               }
               else{
                        this.common.closeLoading();
                        alert("Please check your internet connection");
                        console.log("error");

                    }
                    
                  }, (err) => {
                    //Connection failed message
                    this.common.closeLoading();
                    alert("Please check your internet connection");
                    }); // end of err
                }

  }

  reset_button(){

    this.show_img_icon = true ;
    this.show_create_request = false;

    this.req.request_type = "";
    // this.req.to_costcenter.push ("");

  }
  

   scan(){

    console.log("Open Barcode Scanner");

    this.show_result = false;
    this.show_scanButton = true;

    this.barcodeScanner.scan().then((barcodeData) => {
      // Success! Barcode data is here
      // alert(barcodeData.text);
      
      this.item_barcode = barcodeData.text; 
      this.show_result = true;
      this.show_scanButton = false;

      if(this.item_barcode){
        this.common.presentLoading();
        this.authService.getItemDetail(barcodeData.text).then((result) =>{
            this.resposeData = result;

              // alert(this.resposeData);
              // console.log(this.resposeData);
              let item_id = JSON.stringify(this.resposeData.item_id);
              let item_code = JSON.stringify(this.resposeData.item_code);
              let item_description = JSON.stringify(this.resposeData.item_description);
              let shelf_location = JSON.stringify(this.resposeData.shelf_location);
              let qty_onhand = JSON.stringify(this.resposeData.qty_on_hand);

              this.common.closeLoading();

              if(item_id!=="null"){
                
                alert(item_code+' - '+item_description);
                
                this.show_result = true;
                this.show_scanButton = false;

                 this.itemDetail.item_id =  item_id.substring(1, item_id.length - 1);
                 this.itemDetail.item_code =  item_code.substring(1, item_code.length - 1);
                 this.itemDetail.item_description = item_description.substring(1, item_description.length - 1);
                 this.itemDetail.shelf_location = shelf_location.substring(1, shelf_location.length - 1);
                 this.itemDetail.qty_on_hand = qty_onhand.substring(1, qty_onhand.length - 1);
              
              }
              else{
                alert("Not Found Item");
                console.log("Not Found Item");
               
              }
            
          }, (err) => {
            //Connection failed message
            alert("Please check your internet connection");
            }); // end of err
    }
      
      console.log("Scan OK");
     }, (err) => {
         // An error occurred
         alert("Scan Error");
      console.log("Scan Error");
     });
  }

  submit(){
    // hide item information & show new scan
    this.show_result = false;
    this.show_scanButton = true;

    //alert(this.itemDetail.qty_issue);


    // this.itemDetail.item_id = '1';
    // this.itemDetail.qty_issue = '3';
 

    if(this.itemDetail.qty_issue){
      this.common.presentLoading();
      this.authService.postQtyIssue(this.itemDetail).then((result) =>{
          this.resposeData = result;
          this.common.closeLoading();
          let code_string = JSON.stringify(this.resposeData.code);
          let code =  code_string.substring(1, code_string.length - 1);
          let msg = JSON.stringify(this.resposeData.msg);

          //  alert(this.resposeData);
          //  alert(code);
          
            if(code === "Success"){
              
              // this.notification.beep(1);
              this.common.closeLoading();
              alert('Your Transaction : '+code+' - '+msg);
              

                this.show_result = false;
                this.show_scanButton = true;

                this.itemDetail.item_id = "";
                this.itemDetail.item_code =  "";
                this.itemDetail.item_description = "";
                this.itemDetail.shelf_location = "";
                this.itemDetail.qty_on_hand = "";
                this.itemDetail.qty_issue = "";
                this.itemDetail.note = "";
                this.itemDetail.ref_doc = "";
                
            }
            else{
              this.common.closeLoading();
              alert("Please check your internet connection");
              console.log("error");

            }
          
        }, (err) => {
          //Connection failed message
          this.common.closeLoading();
          alert("Please check your internet connection");
          }); // end of err
      }
  }



  converTime(time) {
    let a = new Date(time * 1000);
    return a;
  }

  backToWelcome() {
    console.log("logout");
    // alert("logout");

    // const root = this.app.getRootNav().setRoot(Welcome);
    this.navCtrl.parent.parent.setRoot(Welcome);
    
    // const root = this.app.getRootNavById();

    // const root = this.app.getRootNav (); // in this line, you have to declare a root, which is the app's root 
    // root.popToRoot (); // here you go to the root.
  
    // const root = this.app.getRootNavs();
    // console.log(root);
    // root.popToRoot();
    // this.navCtrl.setRoot(Welcome);
    
    // // this.navCtrl.push(TabsPage);
    // this.navCtrl.push(Welcome); 
    // this.navCtrl.popAll();
    // this.navCtrl.pop()  
  }

  logout() {
    //Api Token Logout
    // alert("TESTTSET");
    localStorage.clear();
    setTimeout(() => this.backToWelcome(), 100);
  }

  scan_waybill() {
    console.log("waybill");

    this.barcodeScanner.scan().then((barcodeData) => {
      // Success! Barcode data is here
      this.itemDetail.ref_doc = barcodeData.text ;
      this.show_submit = true;
     }, (err) => {
         // An error occurred
      alert("Please check your waybill barcode");
     });


  }

  onChange(){

    console.log("OnChange triger");
    // console.log( this.itemDetail.note);

    if(this.itemDetail.note=='1'){
      console.log("to customer");
      // this.itemDetail.note="to customer";
      this.show_waybill = true;

          // if(this.itemDetail.ref_doc.length == 0){
          //   console.log("waybill is null");
          //   this.show_submit = false;
          // }
            // this.show_submit = false;
      //alert( this.itemDetail.note);
    } else 
          if(this.itemDetail.note=='2'){
              // alert( this.itemDetail.note);
              console.log("to spare");
              // this.itemDetail.note="to spare";
              this.show_waybill = false;
              this.itemDetail.ref_doc = "";
              this.show_submit = true;
         }
     
  } // End of function

  // function input_waybill
  input_waybill(){
    this.show_submit = true;
    console.log("waybill channge");
    console.log(this.itemDetail.ref_doc.length);

    if(this.itemDetail.ref_doc.length >= 1){
      this.show_submit = true;
      console.log("waybill no.xxx");
    } else {
      console.log("no waybill number");
      this.show_submit = false;
    }

    // if(this.itemDetail.ref_doc.length >= 1){
    //     this.show_submit = true;
    //     console.log("waybill no.xxx");
        
    // } 
    //     console.log("no waybill number");
    //     this.show_submit = false;
  } // end of function
  
}
